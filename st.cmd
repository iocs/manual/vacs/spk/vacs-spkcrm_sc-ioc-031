#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Spk-030Crm:VAC-VEG-01100
# Module: vac_ctrl_mks946_937b
# Todo: Check if the port is  as well as serial numbers for SLOT Board
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Spk-030Crm:VAC-VEG-01100, BOARD_A_SERIAL_NUMBER = 1906271455, BOARD_B_SERIAL_NUMBER = 1905061525, BOARD_C_SERIAL_NUMBER = 1905011311, IPADDR = moxa-vac-spk-20-u007.tn.esss.lu.se, PORT = 4001")

#
# Device: Spk-030Crm:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGP-01100, CHANNEL = A1, CONTROLLERNAME = Spk-030Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: Spk-030Crm:Vac-VGC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGC-01100, CHANNEL = B1, CONTROLLERNAME = Spk-030Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-030Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: Spk-040Crm:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGP-01100, CHANNEL = A2, CONTROLLERNAME = Spk-030Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: '")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: Spk-040Crm:Vac-VGC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGC-01100, CHANNEL = C1, CONTROLLERNAME = Spk-030Crm:VAC-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGC-01100, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGC-01100, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGC-01100, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = Spk-040Crm:Vac-VGC-01100, RELAY = 4, RELAY_DESC = 'not wired'")