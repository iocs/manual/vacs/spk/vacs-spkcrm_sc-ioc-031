# VacS-SpkCrm_SC-IOC-031

IOC for Spk-030Crm MKS Gauge Controller

---

## Used Module

IOC uses [e3-vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)

## Devices

**Controller:** 
- Spk-030Crm:VAC-VEG-01100

**Controlled Devices:**
- Spk-030Crm:Vac-VGP-01100
- Spk-030Crm:Vac-VGC-01100
- Spk-040Crm:Vac-VGP-01100
- Spk-040Crm:Vac-VGC-01100

